package com.example.artyom.testgurtam.db;



import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PrimeNumber extends RealmObject {
    @PrimaryKey
    private long id;
    private int number;

    public void setId(long id) {
        this.id = id;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public long getId() {
        return id;
    }
}
