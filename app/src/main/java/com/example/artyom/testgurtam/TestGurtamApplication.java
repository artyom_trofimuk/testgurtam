package com.example.artyom.testgurtam;

import android.app.Application;

import com.example.artyom.testgurtam.helpers.Helper;

public class TestGurtamApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Helper.setHelpers(this);
    }
}
