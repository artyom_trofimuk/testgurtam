package com.example.artyom.testgurtam.helpers;

import android.content.Context;

import com.example.artyom.testgurtam.db.PrimeNumbersRealmRequest;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class Helper {
    private static PrimeNumbersRealmRequest primeNumbersRealmRequest;

    public static void setHelpers(Context context) {
        primeNumbersRealmRequest = new PrimeNumbersRealmRequest();
        Realm.init(context);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder()
                .name("TestGurtam.realm")
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfig);
    }

    public static Realm getDatabase() {
        return Realm.getDefaultInstance();
    }

    public static PrimeNumbersRealmRequest getRealmRequestInstance(){
        return  primeNumbersRealmRequest;
    }

}
