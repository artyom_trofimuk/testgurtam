package com.example.artyom.testgurtam.db;

import com.example.artyom.testgurtam.helpers.Helper;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class PrimeNumbersRealmRequest {
    public void setPrimeNumbers(ArrayList<PrimeNumber> primeNumbers){
        Realm realm = Helper.getDatabase();
        try {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(primeNumbers);
            realm.commitTransaction();
        }catch (Exception e){
            e.printStackTrace();
            if (realm.isInTransaction()){
                realm.cancelTransaction();
            }
        }
    }
    public  RealmResults<PrimeNumber> getPrimeNumbers(){
        Realm realm = Helper.getDatabase();
        RealmResults<PrimeNumber> numbers = null;
        try {
            realm.beginTransaction();
            numbers = realm.where(PrimeNumber.class).findAll();
            realm.commitTransaction();
        }catch (Exception e){
            e.printStackTrace();
            if (realm.isInTransaction()){
                realm.cancelTransaction();
            }
        }
        return numbers;
    }

    public void clearPrimeNumbers(){
        try {
            Realm realm = Helper.getDatabase();
            realm.beginTransaction();
            RealmResults<PrimeNumber> numbers = realm.where(PrimeNumber.class).findAll();
            numbers.deleteAllFromRealm();
            realm.commitTransaction();
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
