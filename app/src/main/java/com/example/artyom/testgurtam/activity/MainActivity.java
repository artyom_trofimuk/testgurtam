package com.example.artyom.testgurtam.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.example.artyom.testgurtam.db.PrimeNumber;

import com.example.artyom.testgurtam.helpers.Helper;
import com.example.artyom.testgurtam.helpers.PrimeGeneratorHelper;
import com.example.artyom.testgurtam.R;
import com.example.artyom.testgurtam.adapters.RecyclerAdapter;
import com.example.artyom.testgurtam.db.PrimeNumbersRealmRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.etLimit)
    EditText etLimit;

    @Bind(R.id.buttonStartGen)
    Button buttonStartGen;

    @Bind(R.id.recyclerViewPrimeNumber)
    RecyclerView recyclerViewPrimeNumber;

    @Bind(R.id.twTimeWork)
    TextView twTimeWork;

    @Bind(R.id.progressBarPrimeNumber)
    ProgressBar progressBarPrimeNumber;

    @Bind(R.id.buttonClearСache)
    Button buttonClearСache;

    private RecyclerAdapter primeNumberAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<PrimeNumber> primeNumberList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        mLayoutManager = new LinearLayoutManager(this);
        recyclerViewPrimeNumber.setLayoutManager(mLayoutManager);
        primeNumberList = new ArrayList<>();
        primeNumberAdapter = new RecyclerAdapter(primeNumberList);
        recyclerViewPrimeNumber.setAdapter(primeNumberAdapter);

        primeNumberList.addAll(Helper.getRealmRequestInstance().getPrimeNumbers());
        buttonStartGen.setOnClickListener(v -> {
            keyBoardHide();
            createPrimeNumbers();
        });
        buttonClearСache.setOnClickListener(v -> {
            Helper.getRealmRequestInstance().clearPrimeNumbers();
            primeNumberList.clear();
            primeNumberAdapter.notifyDataSetChanged();
        });
    }

    public void createPrimeNumbers(){
        Long startTime = new Date().getTime() ;
        if(!etLimit.getText().toString().isEmpty()) {

            primeNumberList.clear();
            progressBarPrimeNumber.setVisibility(View.VISIBLE);
            twTimeWork.setText(getString(R.string.emptyString));

            primeNumberList.addAll(
                    new PrimeGeneratorHelper()
                            .createPrimeNumbers(Integer
                                    .parseInt(etLimit
                                            .getText().toString())));

            primeNumberAdapter.notifyDataSetChanged();
            progressBarPrimeNumber.setVisibility(View.INVISIBLE);

            if (primeNumberList.size()!=0){
                Helper.getRealmRequestInstance().setPrimeNumbers(primeNumberList);
            }

            Long fullTimeWork =  new Date().getTime() - startTime;
            twTimeWork.append(String.valueOf(fullTimeWork) + getString(R.string.timeMs));
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        keyBoardHide();
        return true;
    }

    private void keyBoardHide(){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
