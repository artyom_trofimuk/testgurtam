package com.example.artyom.testgurtam.adapters;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.artyom.testgurtam.R;
import com.example.artyom.testgurtam.db.PrimeNumber;

import java.util.ArrayList;


import butterknife.Bind;
import butterknife.ButterKnife;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RandomViewHolder> {
    private ArrayList<PrimeNumber> primeNumberList;

    public RecyclerAdapter(ArrayList<PrimeNumber> primeNumberList) {
        this.primeNumberList = primeNumberList;
    }

    @Override
    public RecyclerAdapter.RandomViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        View v = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.recycler_item, parent, false);

        return new RecyclerAdapter.RandomViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RandomViewHolder holder, int position) {
        holder.itemView.setTag(position);
        holder.twRecyclerItem.setText(String.valueOf(primeNumberList.get(position).getNumber()));
   }

    @Override
    public int getItemCount() {
        return primeNumberList.size();
    }

    public static class RandomViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.twRecyclerItem)
        TextView twRecyclerItem;

        public RandomViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}