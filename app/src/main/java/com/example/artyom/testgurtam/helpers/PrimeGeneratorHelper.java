package com.example.artyom.testgurtam.helpers;


import com.example.artyom.testgurtam.db.PrimeNumber;

import java.math.BigInteger;
import java.util.ArrayList;

public class PrimeGeneratorHelper {

    public PrimeGeneratorHelper(){}

    public ArrayList<PrimeNumber> createPrimeNumbers(int limit ){
        int id = 0;
        ArrayList<PrimeNumber> primeNumbersList = new ArrayList<>();
        for (int start = 0; start < limit; start++ ){
            if(isPrimeNumber(start)){
                PrimeNumber primeNumber = new PrimeNumber();
                primeNumber.setId(id);
                primeNumber.setNumber(start);
                primeNumbersList.add(primeNumber);
                id++;
            }
        }
        return  primeNumbersList;
    }

    private boolean isPrimeNumber(int number){
        BigInteger bigInt = BigInteger.valueOf(number);
        return bigInt.isProbablePrime(100);
    }
}
